package com.example.fristappmobile.Activitys.modelo;

import androidx.annotation.NonNull;

import com.example.fristappmobile.Activitys.Configuração.ConfiguracaoFirebase;
import com.google.firebase.database.DatabaseReference;

public class Usuario {

    private  String id;
    private String nome;
    private  String email;
    private  String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void salvarDados() {

        DatabaseReference firebase = ConfiguracaoFirebase.getDatabaseReference();
        firebase.child("Usuários").child(this.id).setValue(this);
    }

    @NonNull
    @Override
    public String toString() {
        return "Nome: " + getNome() + "\n" + "Email: " + getEmail() + "\n" + "Password: " + getPassword();
    }
}
