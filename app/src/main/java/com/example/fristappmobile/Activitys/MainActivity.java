package com.example.fristappmobile.Activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fristappmobile.Activitys.modelo.Usuario;
import com.example.fristappmobile.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private Button btnLogar;
    private Button btnCancel;
    private EditText edtEmail;
    private EditText edtPassword;
    private Button btnCadastrar;
    private Usuario u;

    //Firebase
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Firebase
        mAuth = FirebaseAuth.getInstance();

        btnLogar = (Button) findViewById(R.id.btnLogar);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnCadastrar = (Button) findViewById(R.id.btnCadastrar);

        btnLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser(edtEmail.getText().toString(), edtPassword.getText().toString());

            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtEmail.setText("");
                edtPassword.setText("");
            }
        });

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastro();
            }
        });

    }

    private void loginUser(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Log.d("TAG", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(getApplicationContext(), "Login Realizado com Sucesso",
                                    Toast.LENGTH_SHORT).show();
                            openPrincipalActivity();
                        } else {

                            Log.w("TAG", "signInWithEmail:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Falha na Autenticação",
                                    Toast.LENGTH_SHORT).show();


                        }


                    }
                });

    }

    private boolean userConnected(){

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null){
            return false;
        }else{
            return true;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(userConnected()){
            openPrincipalActivity();
        }
    }

    private void openPrincipalActivity(){
        Intent intent = new Intent(getApplication(),PrincipalActivity.class);
        startActivity(intent);
        finish();

    }

    private void cadastro(){
        Intent intent = new Intent(getApplicationContext(), CadastroActivity.class);
        startActivity(intent);
        finish();
    }
}