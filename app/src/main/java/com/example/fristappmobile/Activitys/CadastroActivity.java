package com.example.fristappmobile.Activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fristappmobile.Activitys.modelo.Usuario;
import com.example.fristappmobile.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class CadastroActivity extends AppCompatActivity {
    private EditText edtNome, edtEmail, edtPassword;
    private Button btnSalvar;
    private FirebaseAuth mAuth;
    private Usuario u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        edtNome = (EditText) findViewById(R.id.edtNome);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnSalvar = (Button) findViewById(R.id.btnSalvar);
        mAuth = FirebaseAuth.getInstance();


        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recuperarDados();
                criarLogin();
            }
        });
    }

    private void criarLogin() {
        mAuth.createUserWithEmailAndPassword(u.getEmail(), u.getPassword())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser user = mAuth.getCurrentUser();
                            u.setId(user.getUid());
                            u.salvarDados();
                            startActivity(new Intent(CadastroActivity.this, PrincipalActivity.class));

                        }else {
                            Toast.makeText(CadastroActivity.this, "Erro ao criar usuário",
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    private void recuperarDados() {
        if(edtNome.getText().toString() == "" || edtEmail.getText().toString() == "" ||
                edtPassword.getText().toString() == ""){
            Toast.makeText(this, "Você deve preencher todos os campos", Toast.LENGTH_LONG);
        }else{
            u = new Usuario();
            u.setNome(edtNome.getText().toString());
            u.setEmail(edtEmail.getText().toString());
            u.setPassword(edtPassword.getText().toString());
        }
    }
}