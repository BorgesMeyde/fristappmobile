package com.example.fristappmobile.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.fristappmobile.R;

public class DetalhesListViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_list_view);
        TextView detalhes = (TextView) findViewById(R.id.txtDetalhes);
        Intent intent = getIntent();
        String message = intent.getStringExtra(PrincipalActivity.EXTRA_MESSAGE);
        detalhes.setText(message);

    }
}